====================================================
PROJETO DE BANCO DE DADOS - ENTREGA DE PROVAS
====================================================


FERRAMENTAS UTILIZADAS
----------------------------------------------------
brModelo		- Modelo Conceitual
DBDesigner		- Modelo Relacional
PostgreSQL		- Modelo F�sico
Office 2016		- Dicion�rio de Dados


FERRAMENTAS PARA GERENCIAMENTO DO PROJETO
----------------------------------------------------
Trello
GitLab
Dropbox


TRABALHOS � SEREM REALIZADOS
----------------------------------------------------
OK - Modelo Conceitual
OK - Modelo Relacional
OK - Dicion�rio de Dados
OK - Modelo F�sico
OK - Realizar l�gica de consultas entre as tabelas
OK - Inser��o de Dados
OK - Cria��o de View
OK - Testes
